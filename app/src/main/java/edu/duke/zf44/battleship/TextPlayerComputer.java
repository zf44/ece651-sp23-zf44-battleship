package edu.duke.zf44.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.EOFException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.*;
import java.util.function.Function;

public class TextPlayerComputer extends TextPlayer{

  private final ArrayList<String> actionLst;
  private final ArrayList<String> orientationLst;
  private final int rowRange;
  private final int colRange;
  private final Random random;
  
  public TextPlayerComputer(String textplayer,Board<Character> theBoard, BufferedReader inputSource, PrintStream out, AbstractShipFactory<Character> shipFactory,int numMovesLeft, int numSonarLeft){
    super(textplayer, theBoard, inputSource, out, shipFactory, numMovesLeft, numSonarLeft);
    this.rowRange = theBoard.getHeight();
    this.colRange = theBoard.getWidth();
    this.actionLst = generateAction();
    this.orientationLst = generateOri();
    this.random = new Random();
    random.setSeed(22);
  }

  protected ArrayList<String> generateAction(){
    ArrayList<String> action = new ArrayList<String>();
    action.add("F");
    action.add("M");
    action.add("S");
    return action;
  }
  
  protected ArrayList<String> generateOri(){
    ArrayList<String> action = new ArrayList<String>();
    action.add("H");
    action.add("V");
    action.add("U");
    action.add("D");
    action.add("R");
    action.add("L");
    return action;
  }
  
  protected Coordinate randomCoordinate(){
    int n1 = random.nextInt(rowRange);
    int n2 = random.nextInt(colRange);
    return new Coordinate(n1,n2);
  }
  
  protected Placement randomPlacement(){
    Coordinate c1 = randomCoordinate();
    String s1 = randomOri();
    char o1 = s1.charAt(0);
    return new Placement(c1,o1);
  }
  
  protected String randomOri(){
    int n1 = random.nextInt(6);
    return orientationLst.get(n1);
  }
  
  protected String randomAction(){
    int n1 = random.nextInt(3);
    return actionLst.get(n1);
  }
  
  public String getName(){
    return this.TextPlayer;
  }
  
  public Board<Character> getBoard(){
    return this.theBoard;
  }
  
  protected void setupShipCreationMap(){
    shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
    shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
    shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
    shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
  }

  protected void setupShipCreationList(){
    shipsToPlace.addAll(Collections.nCopies(1, "Submarine"));
    shipsToPlace.addAll(Collections.nCopies(1, "Destroyer"));
    shipsToPlace.addAll(Collections.nCopies(1, "Carrier"));
    shipsToPlace.addAll(Collections.nCopies(1, "Battleship"));
  } 
  
  @Override
  public Placement readPlacement(String prompt) {
    return randomPlacement();
  }
  
  @Override
  public void doOnePlacement(String shipName, Function<Placement, Ship<Character> > createFn) throws IOException {
    boolean isDone = false;
    while (!isDone){
      try{
        Placement p = readPlacement("Player " + TextPlayer + " where do you want to place a " + shipName + "?");
        Ship<Character> s = createFn.apply(p);
        String error = theBoard.tryAddShip(s);
        //I didn't cover this because it test put two ship on the same coordinate, while for randomly place, it is a small possibility.
        //However, I test it in Human's player.
        if (error!=null){
          throw new IllegalArgumentException(error);
        }
        isDone = true;
      }
      catch (IllegalArgumentException exception){
        out.print(exception.getMessage() + "\n");
      }
    }
    out.print(view.displayMyOwnBoard());
  }

  @Override
  public void doPlacementPhase() throws IOException{
    for (String ship: shipsToPlace){
      doOnePlacement(ship,shipCreationFns.get(ship));
    }
  }
  
  @Override
  public char chooseAction() throws IllegalArgumentException{
    String s = randomAction();
    char act = s.charAt(0);
    return act;
  }
  
  @Override
  public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException{
    boolean isDone = false;
    while(!isDone){
      try{
        char action = chooseAction();
        if (action == 'F'){
          tryAttack(enemyBoard);
        }
        if (action == 'M'){
          tryMove();
        }
        if (action == 'S'){
          trySonar(enemyBoard);
        }
      }
      catch(IllegalArgumentException exception){
        out.print(exception.getMessage() + "\n");
        continue;
      }
      isDone = true;
    }
  }
  
  @Override
  public void tryMove() throws IOException, IllegalArgumentException {
    if (moveNum <=0){
      throw new IllegalArgumentException("Illegal action, all moves used.");
    }
    Coordinate c = randomCoordinate();
    Ship<Character> theOriShip = theBoard.getShip(c);
    if (theOriShip == null){
      throw new IllegalArgumentException("This coordinate doesn't existed a ship, try again.");
    }
    Placement p = readPlacement("Where would you like "+ theOriShip.getName()+" move to?");
    Ship<Character> theDstShip = shipCreationFns.get(theOriShip.getName()).apply(p);
    theBoard.tryMoveShip(theOriShip,theDstShip);
    moveNum -= 1;
  }
  
  @Override
  public void tryAttack(Board<Character> enemyBoard) throws IOException, IllegalArgumentException {
      out.print("Computer Turn:\nWhere do you want to hit your enemy? Enter a coordinate:\n");
      Coordinate hitAt = randomCoordinate();
      Ship<Character> shipHitAt = enemyBoard.fireAt(hitAt);
      if (shipHitAt == null) {
        out.print("Computer missed!\n");
      }
      else{
        out.print("Computer hit a " + shipHitAt.getName() + "!\n");
      }
  }
  
  @Override
  public void trySonar(Board<Character> enemyBoard) throws IOException, IllegalArgumentException {
    if (sonarNum <=0){
      throw new IllegalArgumentException("Illegal action, all sonar used.");
    }
    out.print("Computer Turn:\nComputer Place a sonar");
    sonarNum-=1;
  }  
}
