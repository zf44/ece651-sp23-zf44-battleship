package edu.duke.zf44.battleship;
import java.util.*;

/**
 * This interface represents any type of board in our Battleship game. It is
 * generic in typename T, which is the type of information the view needs to
 * display this board.
 */
public interface Board<T> {
  /**
   * Get the width of one board
   * @return the width of that
   */ 
  public int getWidth();
  
  /**
   * Get the height of one board
   * @return the height of that
   */ 
  public int getHeight();
  
  /**
   * Try put a ship onto the board
   * @pram the ship we are going to add onto the board
   * @return if false, return the error
   */ 
  public String tryAddShip(Ship<T> toAdd);

  /**
   * Get the display information of an coordinate
   * @pram a Coordinate
   * @return the infomation which is going to display on board from your perspective
   */
  public T whatIsAtForSelf(Coordinate where);

  /**
   * Try to fire at a given coordinate
   * @pram a Coordinate
   * @return if fire successfully, return the ship that has been fired, else return null
   */
  public Ship<T> fireAt(Coordinate c);
  
  /**
   * Get the display information of an coordinate
   * @pram a Coordinate
   * @return the infomation which is going to display on board from enemy perspective
   */ 
  public T whatIsAtForEnemy(Coordinate where);

  /**
   * Check if all ship for a player is sunk
   * @return if yes, return true, else, return false
   */   
  public boolean isAllSunk();
  
  /**
   * Get the ship which occupied the given coordinate
   * @pram a Coordinate
   * @return a ship
   */   
  public Ship<T> getShip(Coordinate where);
  
  /**
   * Try to move a ship
   * @pram theOriShip is the original ship you need to move from
   * @pram theDstShip is the destination you need to move to
   */  
  public void tryMoveShip(Ship<T> theOriShip, Ship<T> theDstShip);
  
  /**
   * Create a Sonar for ship detection
   * @pram range is sonar's center coordinate
   * @parm coordinate is a coordinate
   * @return a sonar's all coordinates
   */  
  public ArrayList<Coordinate> createSonar(int range, Coordinate where);
  
  /**
   * Use Sonar to detect ship
   * @pram range is range of a sonar
   * @parm coordinate is sonar's center coordinate
   * @return a dictionary about the detection result
   */  
  public HashMap<String, Integer> sonar(int range, Coordinate where);
}

