package edu.duke.zf44.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.EOFException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.*;
import java.util.function.Function;
public class TextPlayer {

  final Board<Character> theBoard;
  final BoardTextView view;
  final BufferedReader inputReader;
  final PrintStream out;
  final AbstractShipFactory<Character> shipFactory;
  final String TextPlayer;
  final ArrayList<String> shipsToPlace;
  final HashMap<String, Function<Placement, Ship<Character> > > shipCreationFns;
  int moveNum;
  int sonarNum;
  
  public TextPlayer(String textplayer,Board<Character> theBoard, BufferedReader inputSource, PrintStream out, AbstractShipFactory<Character> shipFactory,int numMovesLeft, int numSonarLeft){
    this.theBoard = theBoard;
    this.view = new BoardTextView(theBoard);
    this.inputReader = inputSource;
    this.out = out;
    this.shipFactory = shipFactory;
    this.TextPlayer = textplayer;
    this.shipsToPlace = new ArrayList<String>();
    this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character> > >();
    this.moveNum = numMovesLeft;
    this.sonarNum = numSonarLeft;
    setupShipCreationMap();
    setupShipCreationList();
  }

  public String getName(){
    return this.TextPlayer;
  }
  
  public Board<Character> getBoard(){
    return this.theBoard;
  }
  
  public Placement readPlacement(String prompt) throws IOException {
    out.println(prompt);
    String s = inputReader.readLine();
    if (s == null) {
      throw new EOFException("This should fail with EOF");
    }
    return new Placement(s);
  }

  public void doOnePlacement(String shipName, Function<Placement, Ship<Character> > createFn) throws IOException {
    boolean isDone = false;
    while (!isDone){
      try{
        Placement p = readPlacement("Player " + TextPlayer + " where do you want to place a " + shipName + "?");
        Ship<Character> s = createFn.apply(p);
        String error = theBoard.tryAddShip(s);
        if (error!=null){
          throw new IllegalArgumentException(error);
        }
        isDone = true;
      }
      catch (IllegalArgumentException exception){
        out.print(exception.getMessage() + "\n");
      }
    }
    out.print(view.displayMyOwnBoard());
  }

  /***  
  public void doOnePlacement() throws IOException{
    String prompt = "Where would you like to put your ship?";
    Placement r = readPlacement("Player "+ TextPlayer + " " + prompt);
    Coordinate c = r.getWhere();
    Ship<Character> s1 = shipFactory.makeDestroyer(r);
    this.theBoard.tryAddShip(s1);
    out.print(view.displayMyOwnBoard());
  }
  ***/

  public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException{
    BoardTextView enemyV = new BoardTextView(enemyBoard);
    out.print(view.displayMyBoardWithEnemyNextToIt(enemyV, " Your Ocean", enemyName + "'s Ocean") + "\n");
    boolean isDone = false;
    while(!isDone){
      try{
        char action = chooseAction();
        if (action == 'F'){
          tryAttack(enemyBoard);
        }
        if (action == 'M'){
          tryMove();
        }
        if (action == 'S'){
          trySonar(enemyBoard);
        }
      }
      catch(IllegalArgumentException exception){
        out.print(exception.getMessage() + "\n");
        continue;
      }
      isDone = true;
    }
  }
  
  public void tryAttack(Board<Character> enemyBoard) throws IOException, IllegalArgumentException {
      out.print("Where do you want to hit your enemy? Enter a coordinate:\n");
      String s = inputReader.readLine();
      Coordinate hitAt = new Coordinate(s);
      Ship<Character> shipHitAt = enemyBoard.fireAt(hitAt);
      if (shipHitAt == null) {
        out.print("You missed!\n");
      }
      else{
        out.print("You hit a " + shipHitAt.getName() + "!\n");
      }
  }
  
  public void tryMove() throws IOException, IllegalArgumentException {
    if (moveNum <=0){
      throw new IllegalArgumentException("Illegal action, all moves used.");
    }
    out.print("Which ship do you like to move?");
    String s = inputReader.readLine();
    Coordinate c = new Coordinate(s);
    Ship<Character> theOriShip = theBoard.getShip(c);
    if (theOriShip == null){
      throw new IllegalArgumentException("This coordinate doesn't existed a ship, try again.");
    }
    out.print("Where would you like "+ theOriShip.getName()+" move to?");
    String s2 = inputReader.readLine();
    Placement p = new Placement(s2);
    Ship<Character> theDstShip = shipCreationFns.get(theOriShip.getName()).apply(p);
    theBoard.tryMoveShip(theOriShip,theDstShip);
    moveNum -= 1;
  }
  
  public void trySonar(Board<Character> enemyBoard) throws IOException, IllegalArgumentException {
    if (sonarNum <=0){
      throw new IllegalArgumentException("Illegal action, all sonar used.");
    }
    out.print("Where do you like put a sonar?");
    String s = inputReader.readLine();
    Coordinate c = new Coordinate(s);
    HashMap<String, Integer> shipCount = enemyBoard.sonar(3, c);
    
    StringBuilder ans = new StringBuilder();
    ans.append("\n");
    for (String shipName:shipCount.keySet()){
      ans.append(shipName+" occupy " + shipCount.get(shipName) + " squares\n");
    }
    out.print(ans+"\n");
    sonarNum-=1;
  }  
  
  protected void setupShipCreationMap(){
    shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
    shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
    shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
    shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
  }

  protected void setupShipCreationList(){
    shipsToPlace.addAll(Collections.nCopies(1, "Submarine"));
    shipsToPlace.addAll(Collections.nCopies(1, "Destroyer"));
    shipsToPlace.addAll(Collections.nCopies(1, "Carrier"));
    shipsToPlace.addAll(Collections.nCopies(1, "Battleship"));
  }
  
  public void doPlacementPhase() throws IOException{
    out.print(view.displayMyOwnBoard());
    out.print("\n");
    out.print("Player "+TextPlayer+": you are going to place the following ships (which are all\n"+
              "rectangular). For each ship, type the coordinate of the upper left\n"+
              "side of the ship, followed by either H (for horizontal) or V (for\n"+
              "vertical).  For example M4H would place a ship horizontally starting\n"+
              "at M4 and going to the right.  You have\n"+
              "\n"+
              "2 \"Submarines\" ships that are 1x2\n"+
              "3 \"Destroyers\" that are 1x3\n"+
              "3 \"Battleships\" that are 1x4\n"+
              "2 \"Carriers\" that are 1x6\n");
    for (String ship: shipsToPlace){
      doOnePlacement(ship,shipCreationFns.get(ship));
    }
  }

  public char chooseAction() throws IOException,IllegalArgumentException{
    out.print("Possible actions for Player "+TextPlayer+":\n\n"+
              "F Fire at a square\n"+
              "M Move a ship to another square ("+moveNum+" remaining)\n"+
              "S Sonar scan ("+ sonarNum+ " remaining)\n"+
              "\nPlayer"+ TextPlayer+", what would you like to do?\n");
    String s = inputReader.readLine();
    if (s == null){
      throw new EOFException("This should fail with EOF");
    }
    s = s.toUpperCase();
    char choice = s.charAt(0);
    if (choice != 'F'&& choice != 'M' && choice != 'S'){
      throw new IllegalArgumentException("Invalid action\n");
    }
    return choice;
  }
  
  public boolean checkWinLose(){
    return theBoard.isAllSunk();
  }
  
  public void printWin(){
    out.print("Congratulations! Player "+TextPlayer+" win!\n");
  }
  
}
