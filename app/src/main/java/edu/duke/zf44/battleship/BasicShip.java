package edu.duke.zf44.battleship;
import java.util.*;
public abstract class BasicShip<T> implements Ship<T> {

  protected HashMap<Coordinate, Boolean>  myPieces;
  protected ShipDisplayInfo<T> myDisplayInfo;
  protected ShipDisplayInfo<T> enemyDisplayInfo;
  
  
  public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo,ShipDisplayInfo<T> enemyDisplayInfo){
    myPieces = new HashMap<Coordinate, Boolean>();
    for (Coordinate c:where){
      myPieces.put(c,false);
    }
    this.myDisplayInfo = myDisplayInfo;
    this.enemyDisplayInfo = enemyDisplayInfo;
  }
  
  @Override
  public boolean occupiesCoordinates(Coordinate where){
    if (myPieces.get(where) == null){
      return false;
    }
    else{
      return true;
    }
  }

  protected void checkCoordinateInThisShip(Coordinate c){
    if (occupiesCoordinates(c)==false){
      throw new IllegalArgumentException("This coordinate isn't a part of this ship");
    }
  }
  
  @Override
  public boolean isSunk(){
    for (boolean v: myPieces.values()){
      if (v == false){
        return false;
      }
    }
    return true;
  }

  @Override
  public void recordHitAt(Coordinate where){
    checkCoordinateInThisShip(where);
    myPieces.put(where,true);
  }
 
 @Override
  public boolean wasHitAt(Coordinate where){
   checkCoordinateInThisShip(where);
    return myPieces.get(where);
  }

  @Override
  public T getDisplayInfoAt(Coordinate where, boolean isSelf) {
    checkCoordinateInThisShip(where);
    boolean hit = this.wasHitAt(where);
    if (isSelf){
      return myDisplayInfo.getInfo(where, hit);
    }
    else{
      return enemyDisplayInfo.getInfo(where, hit);
    }
  }

  @Override
  public Set<Coordinate> getCoordinates(){
    return myPieces.keySet();
  }
}
