/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package edu.duke.zf44.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;

public class App {  
  TextPlayer player1;
  TextPlayer player2;
  
  public App(TextPlayer player1, TextPlayer player2){
    this.player1 = player1;
    this.player2 = player2;
  }

  public void doPlacementPhase() throws IOException{
    player1.doPlacementPhase();
    player2.doPlacementPhase();
  }

  public void doAttackingPhase() throws IOException{
    while (true){
      player1.playOneTurn(player2.getBoard(), player2.getName());
      if (player2.checkWinLose()){
        player1.printWin();
        break;
      }
      player2.playOneTurn(player1.getBoard(), player1.getName());
      if (player1.checkWinLose()){
        player2.printWin();
        break;
      }
    }
  }
  
  public static void main(String[] args)throws IOException {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20,'X');
    Board<Character> b2 = new BattleShipBoard<Character>(10, 20,'X');
    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    V2ShipFactory factory = new V2ShipFactory();
    PrintStream out = System.out;
    TextPlayer p1;
    TextPlayer p2;
    while(true){
      System.out.print("Who will play Player A?\nChoose: (A) Human  (B)AI:\n");
      String s = input.readLine();
      s = s.toUpperCase();
      char choice = s.charAt(0);
      if (choice == 'A'){
        p1 = new TextPlayer("A", b1, input, out, factory,1,1);
        break;
      }
      if (choice == 'B'){
        p1 = new TextPlayerComputer("A", b1, input, out, factory,1,1);
        break;
      }
      else{
        System.out.print("Invalid player type, choose again");
      }
    }
    
    while(true){
      System.out.print("Who will play Player B?\nChoose: (A) Human  (B)AI:\n");
      String s = input.readLine();
      s = s.toUpperCase();
      char choice = s.charAt(0);
      if (choice == 'A'){
        p2 = new TextPlayer("B", b2, input, out, factory,1,1);
        break;
      }
      if (choice == 'B'){
        p2 = new TextPlayerComputer("B", b2, input, out, factory,1,1);
        break;
      }
      else{
        System.out.print("Invalid player type, choose again");
      }
    }
    
    App app = new App(p1,p2);
    app.doPlacementPhase();
    app.doAttackingPhase();
  }
}
