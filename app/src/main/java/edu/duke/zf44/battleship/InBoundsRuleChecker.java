package edu.duke.zf44.battleship;
import java.util.*;
public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {

  @Override
  protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
    // TODO Auto-generated method stub
    Iterable<Coordinate> s1_coor = theShip.getCoordinates();
    for (Coordinate s:s1_coor){
      String error = this.checkMyRule(s,theBoard);
      if (error != null){
        return error;
      }
    }
    return null;
  }

  @Override
  protected String checkMyRule(Coordinate theCoordinate, Board<T> theBoard){
    if (theCoordinate.getRow() < 0) {
      return "That placement is invalid: the ship goes off the top of the board.";
    }
    if (theCoordinate.getRow() >= theBoard.getHeight()) {
      return "That placement is invalid: the ship goes off the bottom of the board.";
    }
    if (theCoordinate.getColumn() < 0) {
      return "That placement is invalid: the ship goes off the left of the board.";
    }
    if (theCoordinate.getColumn() >= theBoard.getWidth()) {
      return "That placement is invalid: the ship goes off the right of the board.";
    }
    return null;
  }
  
  //we only need to check whether destination after movement is still in bound
  @Override
  protected String checkMyRule(Ship<T> theDstShip, Ship<T> theOriShip, Board<T> theBoard){
    return checkMyRule(theDstShip, theBoard);
  }
  
  public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
    super(next);
  }

}
