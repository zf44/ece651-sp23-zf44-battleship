package edu.duke.zf44.battleship;
import java.util.*;
public class RectangleShip<T> extends BasicShip<T> {
  private final String name;
  private final Coordinate upperLeft;
  private final char orientation;
  private final String type;
  
  public String getName(){return this.name;}
  public Coordinate getUpperLeft(){return this.upperLeft;}
  public char getOrientation(){return this.orientation;}
  public String getType(){return this.type;}
  
  public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
    this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),new SimpleShipDisplayInfo<T>(null, data));
  }
  
  public RectangleShip(Coordinate upperLeft, T data, T onHit) {
    this("testship", upperLeft, 1, 1, data, onHit);
  }

  public RectangleShip(String name, Coordinate upperLeft, int width, int height, SimpleShipDisplayInfo<T> myinfo, SimpleShipDisplayInfo<T> enemyinfo) {
    super(makeCoords(upperLeft, width, height),myinfo,enemyinfo);
    this.name = name;
    this.upperLeft = upperLeft;
    this.orientation = 'N';
    this.type = "rec";
  }

  static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height){
    HashSet<Coordinate> set = new HashSet<Coordinate> ();
    //set.add(upperLeft);
    for (int i = 0; i < height; i++){
      for (int j = 0; j < width; j++){
        Coordinate cur_c = new Coordinate(upperLeft.getRow()+i,upperLeft.getColumn()+j);
        set.add(cur_c);
      }
    }
    return set;

  }
}
