package edu.duke.zf44.battleship;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {

  @Override
  protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
   Iterable<Coordinate> s1_coor = theShip.getCoordinates();
   for (Coordinate s:s1_coor){
     if (theBoard.whatIsAtForSelf(s) != null){
       return "That placement is invalid: the ship overlaps another ship.";
     }
   }
   return null;
  }

  @Override
  protected String checkMyRule(Coordinate theCoordinate, Board<T> theBoard){
    return null;
  }
  
  @Override
  protected String checkMyRule(Ship<T> theDstShip, Ship<T> theOriShip, Board<T> theBoard){
    Iterable<Coordinate> s1_coor = theDstShip.getCoordinates();
    for (Coordinate s:s1_coor){
      if (!theOriShip.occupiesCoordinates(s) && theBoard.whatIsAtForSelf(s) != null){
        return "That placement is invalid, the destination ship overlaps another ship.";
      }
    }
    //if (checkMyRule(theDstShip, theBoard) != null){
    //  return checkMyRule(theDstShip, theBoard);
    //}
    return null;
  }
  
  public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
    super(next);
  }
}
