package edu.duke.zf44.battleship;

public class Placement {
  private final Coordinate where;
  private final char orientation;

  public Placement(String w, char o){
    this.where = new Coordinate(w);
    this.orientation = Character.toUpperCase(o);
  }
  
  public Placement(Coordinate w,char o){
    this.where = w;
    this.orientation = Character.toUpperCase(o);
  }

  public Coordinate getWhere(){return this.where;}
  public char getOrientation(){return this.orientation;}

  @Override
  public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
      Placement c = (Placement) o;
      return where.equals(c.where) && orientation == c.orientation;
    }
    return false;
  }

  @Override
  public String toString() {
    return "("+where.toString()+", " + orientation+")";
  }
  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  public Placement(String descr){
    if (descr.length()!=3){
      throw new IllegalArgumentException("Coordinate must have three letter or number");
    }
    descr = descr.toUpperCase();
    String dest = descr.substring(0,2);
    this.where = new Coordinate(dest);
    char orien = descr.charAt(2);
    this.orientation = orien;
  }
}
