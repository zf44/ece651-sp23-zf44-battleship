package edu.duke.zf44.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T>{
  private T mydata;
  private T onHit;
  public SimpleShipDisplayInfo(T mydata, T onHit){
    this.mydata = mydata;
    this.onHit = onHit;
  }
  
  @Override
  public T getInfo(Coordinate where, boolean hit){
    if (hit){
      return onHit;
    }
    else{
      return mydata;
    }
  }
}
