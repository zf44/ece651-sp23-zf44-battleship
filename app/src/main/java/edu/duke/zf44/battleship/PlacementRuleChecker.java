package edu.duke.zf44.battleship;

public abstract class PlacementRuleChecker<T> {
  private final PlacementRuleChecker<T> next;
  //more stuff

  public PlacementRuleChecker(PlacementRuleChecker<T> next) {
    this.next = next;
  }
  
  
  public String checkPlacement (Ship<T> theShip, Board<T> theBoard) {
    String error = checkMyRule(theShip, theBoard);
    //if we fail our own rule: stop the placement is not legal
    if (error != null) {
      return error;
    }
    //other wise, ask the rest of the chain.
    if (next != null) {
      return next.checkPlacement(theShip, theBoard); 
    }
    //if there are no more rules, then the placement is legal
    return null;
  }
  
  public String checkPlacement (Coordinate theCoordinate, Board<T> theBoard){
    String error = checkMyRule(theCoordinate, theBoard);
    if (error != null) {
      return error;
    }
    if (next != null){
      return next.checkMyRule(theCoordinate, theBoard);
    }
    return null;
  }
  
  /***
    check for move ship
  ***/
  public String checkPlacement (Ship<T> theDstShip, Ship<T> theOriShip, Board<T> theBoard) {
    String error = checkMyRule(theDstShip, theOriShip, theBoard);
    if (error != null) {
      return error;
    }
    if (next != null) {
      return next.checkPlacement(theDstShip, theOriShip, theBoard); 
    }
    return null;
  }
  
  protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);
  protected abstract String checkMyRule(Coordinate theCoordinate, Board<T> theBoard);
  protected abstract String checkMyRule(Ship<T> theDstShip, Ship<T> theOriShip, Board<T> theBoard);
}

