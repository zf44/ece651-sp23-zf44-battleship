package edu.duke.zf44.battleship;

public interface ShipDisplayInfo<T> {
       public T getInfo(Coordinate where, boolean hit);
}

