package edu.duke.zf44.battleship;
import java.util.*;

public class BattleShip<T> extends BasicShip<T> {
  private final String name;
  private final Coordinate upperLeft;
  private final char orientation;
  private final String type;
  
  public String getName(){return this.name;}
  public Coordinate getUpperLeft(){return this.upperLeft;}
  public char getOrientation(){return this.orientation;}  
  public String getType(){return this.type;}
  
  public BattleShip(String name, Coordinate upperLeft, T data, T onHit, char orientation) {
    this(name, upperLeft, new SimpleShipDisplayInfo<T>(data, onHit),new SimpleShipDisplayInfo<T>(null, data),orientation);
  }

  public BattleShip(String name, Coordinate upperLeft, SimpleShipDisplayInfo<T> myinfo, SimpleShipDisplayInfo<T> enemyinfo, char orientation) {
    super(makeCoords(upperLeft, orientation),myinfo,enemyinfo);
    this.name = name;
    this.upperLeft = upperLeft;
    this.orientation = orientation;
    this.type = "battleShip";
  }

  static HashSet<Coordinate> makeCoords(Coordinate upperLeft, char orientation){
    HashSet<Coordinate> set = new HashSet<Coordinate> ();
    if (orientation == 'U') {
      set.add(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn()+0));
      set.add(new Coordinate(upperLeft.getRow()+0,upperLeft.getColumn()+1));
      set.add(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn()+1));
      set.add(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn()+2));
    }
    if (orientation == 'R') {
      set.add(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn()+1));
      set.add(new Coordinate(upperLeft.getRow()+0,upperLeft.getColumn()+0));
      set.add(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn()+0));
      set.add(new Coordinate(upperLeft.getRow()+2,upperLeft.getColumn()+0));
    }
    if (orientation == 'D') { 
      set.add(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn()+1));
      set.add(new Coordinate(upperLeft.getRow()+0,upperLeft.getColumn()+0));
      set.add(new Coordinate(upperLeft.getRow()+0,upperLeft.getColumn()+2));
      set.add(new Coordinate(upperLeft.getRow()+0,upperLeft.getColumn()+1));
    }
    if (orientation == 'L') { 
      set.add(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn()+1));
      set.add(new Coordinate(upperLeft.getRow()+1,upperLeft.getColumn()+0));
      set.add(new Coordinate(upperLeft.getRow()+2,upperLeft.getColumn()+1));
      set.add(new Coordinate(upperLeft.getRow()+0,upperLeft.getColumn()+1));
    }
    return set;
  }
}
