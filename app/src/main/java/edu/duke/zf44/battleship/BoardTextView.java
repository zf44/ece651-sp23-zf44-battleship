package edu.duke.zf44.battleship;
import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the 
 * enemy's board.
 */
public class BoardTextView {
  /**
   * The Board to display
   */
  private final Board<Character> toDisplay;
  /**
   * Constructs a BoardView, given the board it will display.
   * @param toDisplay is the Board to display
   * @throws IllegalArgumentException if the board is larger than 10x26.
   */
  public BoardTextView(Board<Character> toDisplay) {
    this.toDisplay = toDisplay;
    if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
      throw new IllegalArgumentException("Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
    }
  }

  protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
    StringBuilder ans = new StringBuilder("");
    StringBuilder header = new StringBuilder("  ");
    String sep="";
    for (int i = 0; i < toDisplay.getWidth(); i++){
      header.append(sep);
      header.append(i);
      sep = "|";
    }
    header.append("\n");
    ans.append(header);
    for (int i = 0; i < toDisplay.getHeight(); i++){
      sep = "";
      ans.append((char)(i+'A'));
      ans.append(" ");
      for (int j = 0; j < toDisplay.getWidth(); j++){
        ans.append(sep);
        Coordinate c = new Coordinate(i,j);
        if (getSquareFn.apply(c) == null ){
          ans.append(" ");
        }
        else{
          ans.append(getSquareFn.apply(c));
        }
        sep = "|";
      }
      ans.append(" ");
      ans.append((char)(i+'A'));
      ans.append("\n");
    }
    ans.append(header);
    return ans.toString(); //this is a placeholder for the moment
 }
 /**
   * This makes the header line, e.g. 0|1|2|3|4\n
   * 
   * @return the String that is the header line for the given board
   */
  String makeHeader() {
    StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
    String sep=""; //start with nothing to separate, then switch to | to separate
    for (int i = 0; i < toDisplay.getWidth(); i++) {
      ans.append(sep);
      ans.append(i);
      sep = "|";
    }
    ans.append("\n");
    return ans.toString();
  }

  public String displayMyOwnBoard() {
    return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
  }

  public String displayEnemyBoard(){
    return displayAnyBoard((c) -> toDisplay.whatIsAtForEnemy(c));
  }

  public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
    StringBuilder ans = new StringBuilder("     ");//README shows five spaces at
    ans.append(myHeader);
    for (int i = 0; i < (2 * toDisplay.getWidth() + 22 - myHeader.length() - 5); i++){
      ans.append(" ");
    }
    ans.append(enemyHeader);
    ans.append("\n");
    String[] selfLst = displayMyOwnBoard().split("\n");
    String[] enemyLst = enemyView.displayEnemyBoard().split("\n");
    for (int i = 0; i < selfLst.length; i++){
      ans.append(selfLst[i]);
      ans.append("                ");
      if (i == 0 || i == selfLst.length - 1){
        ans.append("  ");
      }
      ans.append(enemyLst[i]);
      ans.append("\n");
    }
    return ans.toString();
  }
}
