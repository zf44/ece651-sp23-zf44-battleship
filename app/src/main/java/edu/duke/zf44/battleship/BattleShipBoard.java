package edu.duke.zf44.battleship;
import java.util.ArrayList;
import java.util.*;
import java.lang.*;
import java.util.HashMap;

public class BattleShipBoard<T> implements Board<T>{
  private int width;
  public int getWidth(){return this.width;};
  private final int height;
  public int getHeight(){return this.height;};
  private final ArrayList<Ship<T>> myShips;
  private final PlacementRuleChecker<T> placementChecker;
  private final HashSet<Coordinate> enemyMisses;
  private final T missInfo;
  
  public BattleShipBoard(int w, int h, T missInfo) {
    this(w, h, new NoCollisionRuleChecker<>(new InBoundsRuleChecker<T>(null)),missInfo);
  }

  public BattleShipBoard(int w, int h, PlacementRuleChecker<T> checker, T missInfo) {
    if (w <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
    }
    if (h <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
    }
    this.width = w;
    this.height = h;
    this.myShips = new ArrayList<Ship<T>>();
    this.placementChecker = checker;
    this.enemyMisses = new HashSet<Coordinate>();
    this.missInfo = missInfo;
  }
  
  public String tryAddShip(Ship<T> toAdd){
    String error = placementChecker.checkPlacement(toAdd, this);
    if (error == null){
      this.myShips.add(toAdd);
    }
    return error;
  }

  public T whatIsAtForSelf(Coordinate where) {
    return whatIsAt(where, true);
  }

  public T whatIsAtForEnemy(Coordinate where) {
    return whatIsAt(where, false);
  }

  protected T whatIsAt(Coordinate where, boolean isSelf){
    if (where.getColumn() > width - 1 || where.getRow() > height - 1) {
      throw new IllegalArgumentException("The coordinate is out of bound in board!");
    }
    if (!isSelf && enemyMisses.contains(where)) {
      return missInfo;
    }
    for (Ship<T> s: myShips) {
      if (s.occupiesCoordinates(where)){
        return s.getDisplayInfoAt(where,isSelf);
      }
    }
    return null;
  }
    
  public Ship<T> fireAt(Coordinate c){
    String error = placementChecker.checkPlacement(c, this);
    if (error != null){
      throw new IllegalArgumentException(error);
    }
    if (whatIsAtForSelf(c)!=null){
      for (Ship<T> s: myShips){
        if(s.occupiesCoordinates(c)){
          s.recordHitAt(c);
          return s;
        }
      }
    }
    enemyMisses.add(c);
    return null;
  }
  
  public boolean isAllSunk() {
    for (Ship<T> s: myShips){
      if (!s.isSunk()){
        return false;
      }
    }
    return true;
  }

  public Ship<T> getShip(Coordinate where){
    for (Ship<T> s: myShips){
      if (s.occupiesCoordinates(where)){
        return s;
      }
    }
    return null;
  }

/***
  Use for coordinate transform for Carrier ships
***/
  protected HashMap<Coordinate,Integer> carrierTransC(char orientation){
    if (orientation == 'U'){
      HashMap<Coordinate,Integer> UTable = new HashMap<Coordinate,Integer>();
      UTable.put(new Coordinate(0,0),0);
      UTable.put(new Coordinate(1,0),1);
      UTable.put(new Coordinate(2,0),2);
      UTable.put(new Coordinate(3,0),3);
      UTable.put(new Coordinate(2,1),4);
      UTable.put(new Coordinate(3,1),5);
      UTable.put(new Coordinate(4,1),6);
      return UTable;
    }
    if (orientation == 'R'){
      HashMap<Coordinate,Integer> RTable = new HashMap<Coordinate,Integer>();
      RTable.put(new Coordinate(0,4),0);
      RTable.put(new Coordinate(0,3),1);
      RTable.put(new Coordinate(0,2),2);
      RTable.put(new Coordinate(0,1),3);
      RTable.put(new Coordinate(1,2),4);
      RTable.put(new Coordinate(1,1),5);
      RTable.put(new Coordinate(1,0),6);
      return RTable;
    }
    if (orientation == 'D'){
      HashMap<Coordinate,Integer> DTable = new HashMap<Coordinate,Integer>();
      DTable.put(new Coordinate(4,1),0);
      DTable.put(new Coordinate(3,1),1);
      DTable.put(new Coordinate(2,1),2);
      DTable.put(new Coordinate(3,1),3);
      DTable.put(new Coordinate(2,0),4);
      DTable.put(new Coordinate(1,0),5);
      DTable.put(new Coordinate(0,0),6);
      return DTable;
    }
    //if (orientation == 'L'){
      HashMap<Coordinate,Integer> LTable = new HashMap<Coordinate,Integer>();
      LTable.put(new Coordinate(1,0),0);
      LTable.put(new Coordinate(1,1),1);
      LTable.put(new Coordinate(1,2),2);
      LTable.put(new Coordinate(1,3),3);
      LTable.put(new Coordinate(0,2),4);
      LTable.put(new Coordinate(0,3),5);
      LTable.put(new Coordinate(0,4),6);
      return LTable;
  }

/***
  Use for coordinate transform for Battle ships
***/  
  protected HashMap<Coordinate,Integer> battleTransC(char orientation){
    if (orientation == 'U'){
      HashMap<Coordinate,Integer> UTable = new HashMap<Coordinate,Integer>();
      UTable.put(new Coordinate(0,1),0);
      UTable.put(new Coordinate(1,0),1);
      UTable.put(new Coordinate(1,1),2);
      UTable.put(new Coordinate(1,2),3);
      return UTable;
    }    
    if (orientation == 'R'){
      HashMap<Coordinate,Integer> RTable = new HashMap<Coordinate,Integer>();
      RTable.put(new Coordinate(1,1),0);
      RTable.put(new Coordinate(0,0),1);
      RTable.put(new Coordinate(1,0),2);
      RTable.put(new Coordinate(2,0),3);
      return RTable;
    }
    if (orientation == 'D'){
      HashMap<Coordinate,Integer> DTable = new HashMap<Coordinate,Integer>();
      DTable.put(new Coordinate(1,1),0);
      DTable.put(new Coordinate(0,2),1);
      DTable.put(new Coordinate(0,1),2);
      DTable.put(new Coordinate(0,0),3);
      return DTable;
    }
    //if (orientation == 'L'){
      HashMap<Coordinate,Integer> LTable = new HashMap<Coordinate,Integer>();
      LTable.put(new Coordinate(1,0),0);
      LTable.put(new Coordinate(2,1),1);
      LTable.put(new Coordinate(1,1),2);
      LTable.put(new Coordinate(0,1),3);
      return LTable;       
  }
  
  public void tryMoveShip(Ship<T> theOriShip, Ship<T> theDstShip){
    String error = placementChecker.checkPlacement(theDstShip, theOriShip, this);
    ArrayList<Coordinate> relatedHit = new ArrayList<Coordinate>();
    Coordinate oriUpperLeft = theOriShip.getUpperLeft();
    Coordinate dstUpperLeft = theDstShip.getUpperLeft();
    
    String shipType = theOriShip.getType();
    for (Coordinate c: theOriShip.getCoordinates()){
      if (theOriShip.wasHitAt(c) == true){
        Coordinate relatedC = new Coordinate(c.getRow()-oriUpperLeft.getRow(),c.getColumn()-oriUpperLeft.getColumn());
        relatedHit.add(relatedC);
      }
    }
    if (error == null){
      this.myShips.remove(theOriShip);
      this.myShips.add(theDstShip);
    }
    else{
      throw new IllegalArgumentException(error); 
    }
    
    for (Coordinate h: relatedHit){
      if (theOriShip.getOrientation() == theDstShip.getOrientation()){
        Coordinate dstC = new Coordinate(dstUpperLeft.getRow()+h.getRow(),dstUpperLeft.getColumn()+h.getColumn());
        theDstShip.recordHitAt(dstC);
      }
      else if (shipType.equals("carrier")){
        HashMap<Coordinate,Integer> pre = carrierTransC(theOriShip.getOrientation());
        HashMap<Coordinate,Integer> cur = carrierTransC(theDstShip.getOrientation());
        Integer index = pre.get(h);
        for (Coordinate c1 : cur.keySet()){
          if (cur.get(c1) == index ){
            theDstShip.recordHitAt(new Coordinate((dstUpperLeft.getRow()+c1.getRow()),(dstUpperLeft.getColumn()+c1.getColumn())));
            break;
          }
        }
      }
      else{//shipType == "battleShip"
        HashMap<Coordinate,Integer> pre = battleTransC(theOriShip.getOrientation());
        HashMap<Coordinate,Integer> cur = battleTransC(theDstShip.getOrientation());
        Integer index = pre.get(h);
        for (Coordinate c1 : cur.keySet()){
          if (cur.get(c1) == index ){
            theDstShip.recordHitAt(new Coordinate((dstUpperLeft.getRow()+c1.getRow()),(dstUpperLeft.getColumn()+c1.getColumn())));
            break;
          }
        }
      }
    }
  }
  
  public ArrayList<Coordinate> createSonar(int range, Coordinate where) throws IllegalArgumentException {
 	  String error = placementChecker.checkPlacement(where, this);
    if (error != null){
      throw new IllegalArgumentException(error);
    }
    ArrayList<Coordinate> sonar = new ArrayList<Coordinate>();
    for (int row = -range; row <= range; row++) {
      int column = Math.abs(range - Math.abs(row));
      for (int col = -column; col <= column; col ++){
        Coordinate cur_c = new Coordinate(where.getRow()+row,where.getColumn()+col);
        sonar.add(cur_c);
      }
    }
    return sonar;
  }
  
  public HashMap<String, Integer> sonar(int range, Coordinate where) throws IllegalArgumentException {
    ArrayList<Coordinate> sonarRange = createSonar(range,where);
    HashMap<String, Integer> shipCount = new HashMap<String, Integer>();
    shipCount.put("Submarine",0);  
    shipCount.put("Destroyer",0);
    shipCount.put("BattleShip",0);
    shipCount.put("Carrier",0);  
    //return shipCount;
    
    for (Ship<T> ship : myShips){
      int count = 0;
      for (Coordinate c : sonarRange) {
        if (ship.occupiesCoordinates(c)){
          count +=1;
        }
      }
      shipCount.put(ship.getName(), count);
    }
    
    return shipCount;
    
  }
}

