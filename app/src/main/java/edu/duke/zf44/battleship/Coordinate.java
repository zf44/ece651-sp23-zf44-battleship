package edu.duke.zf44.battleship;

public class Coordinate {
  private final int row;
  private final int column;
  public int getRow(){return this.row;}
  public int getColumn(){return this.column;}
  public Coordinate(int r, int c){
    this.row = r;
    this.column = c;
  }

  @Override
  public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
      Coordinate c = (Coordinate) o;
      return row == c.row && column == c.column;
    }
    return false;
  }

  @Override
  public String toString() {
    return "("+row+", " + column+")";
  }
  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  public Coordinate(String descr){
    descr = descr.toUpperCase();
    if (descr.length()!=2){
      throw new IllegalArgumentException("Coordinate must have two number, first is between A to Z and second is between 0 to 9");
    }
    //char row = descr.charAt(0);
    int rowLetter = descr.charAt(0);
    int columnLetter = descr.charAt(1);
    if (rowLetter < 'A' || rowLetter > 'Z'){
      throw new IllegalArgumentException("row letter must between A to Z");
    }
    if (columnLetter < '0' || columnLetter > '9'){
      throw new IllegalArgumentException("column letter must between 0 to 9");
    }
    this.row = rowLetter-'A';
    this.column = columnLetter-'0';
  }
}
