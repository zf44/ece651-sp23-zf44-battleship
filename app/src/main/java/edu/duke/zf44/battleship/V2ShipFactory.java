package edu.duke.zf44.battleship;

public class V2ShipFactory implements AbstractShipFactory<Character> {
  @Override
  public Ship<Character> makeSubmarine(Placement where){
    return createShip(where, 1, 2, 's', "Submarine");
  }

  @Override
  public Ship<Character> makeDestroyer(Placement where){
    return createShip(where, 1, 3, 'd', "Destroyer");
  }

  protected boolean checkOrientation(char orientation){
    if (orientation != 'U' && orientation != 'R' && orientation != 'L' && orientation != 'D'){
      return false;
    }
    return true;
  }

  @Override
  public Ship<Character> makeBattleship(Placement where) {
    if (checkOrientation(where.getOrientation()) == false){
      throw new IllegalArgumentException("Invalid orientation, only accept U R D L.");
    }
    return new BattleShip<Character>("BattleShip",where.getWhere(),'b','*',where.getOrientation());
  }

  @Override
  public Ship<Character> makeCarrier(Placement where){
    if (checkOrientation(where.getOrientation()) == false){
      throw new IllegalArgumentException("Invalid orientation, only accept U R D L.");
    }
    return new Carrier<Character>("Carrier",where.getWhere(),'c','*',where.getOrientation());
  }

  protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name){
    if (where.getOrientation() == 'V'){
      return new RectangleShip<Character>(name,where.getWhere(),w,h, new SimpleShipDisplayInfo<Character>(letter, '*'), new SimpleShipDisplayInfo<Character>(null, letter));
    }
    else if (where.getOrientation() == 'H'){
      return new RectangleShip<Character>(name,where.getWhere(),h,w, new SimpleShipDisplayInfo<Character>(letter, '*'), new SimpleShipDisplayInfo<Character>(null, letter));
    }
    else{
      throw new IllegalArgumentException("Invalid orientation, only accept H V.");
    }
  }
}
