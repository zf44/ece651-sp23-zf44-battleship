package edu.duke.zf44.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class CarrierTest {
  @Test
  public void test_carrier() {
    Coordinate upperLeft = new Coordinate(0,0);
    Carrier<Character> car1 = new Carrier<Character>("Carrier",upperLeft, new SimpleShipDisplayInfo<Character>('s', '*'),new SimpleShipDisplayInfo<Character>(null, 's'),'U');
    assertEquals(false,car1.occupiesCoordinates(new Coordinate(5,0)));
    assertEquals(true,car1.occupiesCoordinates(new Coordinate(0,0)));
    assertEquals(true,car1.occupiesCoordinates(new Coordinate(1,0)));
    assertEquals(true,car1.occupiesCoordinates(new Coordinate(2,0)));
    assertEquals(true,car1.occupiesCoordinates(new Coordinate(3,0)));
    assertEquals(true,car1.occupiesCoordinates(new Coordinate(2,1)));
    assertEquals(true,car1.occupiesCoordinates(new Coordinate(3,1)));
    assertEquals(true,car1.occupiesCoordinates(new Coordinate(4,1)));

    Carrier<Character> car2 = new Carrier<Character>("Carrier",upperLeft, 's','*','R');
    assertEquals(true,car2.occupiesCoordinates(new Coordinate(0,1)));
    assertEquals(true,car2.occupiesCoordinates(new Coordinate(0,2)));
    assertEquals(true,car2.occupiesCoordinates(new Coordinate(0,3)));
    assertEquals(true,car2.occupiesCoordinates(new Coordinate(0,4)));
    assertEquals(true,car2.occupiesCoordinates(new Coordinate(1,0)));
    assertEquals(true,car2.occupiesCoordinates(new Coordinate(1,1)));
    assertEquals(true,car2.occupiesCoordinates(new Coordinate(1,2)));

    Carrier<Character> car3 = new Carrier<Character>("Carrier",upperLeft, 's','*','D');
    assertEquals(true,car3.occupiesCoordinates(new Coordinate(0,0)));
    assertEquals(true,car3.occupiesCoordinates(new Coordinate(1,0)));
    assertEquals(true,car3.occupiesCoordinates(new Coordinate(2,0)));
    assertEquals(true,car3.occupiesCoordinates(new Coordinate(1,1)));
    assertEquals(true,car3.occupiesCoordinates(new Coordinate(2,1)));
    assertEquals(true,car3.occupiesCoordinates(new Coordinate(3,1)));
    assertEquals(true,car3.occupiesCoordinates(new Coordinate(4,1)));
    
    Carrier<Character> car4 = new Carrier<Character>("Carrier",upperLeft, 's','*','L');
    assertEquals(true,car4.occupiesCoordinates(new Coordinate(0,2)));
    assertEquals(true,car4.occupiesCoordinates(new Coordinate(0,3)));
    assertEquals(true,car4.occupiesCoordinates(new Coordinate(0,4)));
    assertEquals(true,car4.occupiesCoordinates(new Coordinate(1,0)));
    assertEquals(true,car4.occupiesCoordinates(new Coordinate(1,1)));
    assertEquals(true,car4.occupiesCoordinates(new Coordinate(1,2)));
    assertEquals(true,car4.occupiesCoordinates(new Coordinate(1,3)));
    
    assertEquals("Carrier",car1.getName());
    assertEquals(upperLeft,car1.getUpperLeft());
    assertEquals('R',car2.getOrientation());
    assertEquals("carrier",car1.getType());
  }

}
