package edu.duke.zf44.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class SimpleShipDisplayInfoTest {
  @Test
  public void test_simple() {
    SimpleShipDisplayInfo simple = new SimpleShipDisplayInfo('F','T');
    Coordinate c1 = new Coordinate(1,2);
    assertEquals('T',simple.getInfo(c1,true));
    
  }

}
