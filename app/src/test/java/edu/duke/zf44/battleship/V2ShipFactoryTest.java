package edu.duke.zf44.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V2ShipFactoryTest {

  private void checkShip(Ship<Character> testShip, String expectedName,
                         char expectedLetter, Coordinate... expectedLocs){
    assertEquals(expectedName,testShip.getName());
    for (Coordinate c: expectedLocs){
      assertEquals(true,testShip.occupiesCoordinates(c));
    }
    assertEquals(expectedLetter,testShip.getDisplayInfoAt(expectedLocs[0],true));
    
  }

  @Test
  public void test_create_ship() {
    V2ShipFactory f = new V2ShipFactory();
    
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst = f.makeDestroyer(v1_2);
    checkShip(dst, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));
    Ship<Character> sub = f.makeSubmarine(v1_2);
    checkShip(sub, "Submarine", 's', new Coordinate(1, 2), new Coordinate(2, 2));

    Placement h1_2 = new Placement(new Coordinate(1, 2), 'H');
    Ship<Character> dst_h = f.makeDestroyer(h1_2);
    checkShip(dst_h, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));
    Ship<Character> sub_h = f.makeSubmarine(h1_2);
    checkShip(sub_h, "Submarine", 's', new Coordinate(1, 2), new Coordinate(1, 3));

    assertThrows(IllegalArgumentException.class,()->f.makeSubmarine(new Placement(new Coordinate(1, 2), 'U')));
    assertThrows(IllegalArgumentException.class,()->f.makeBattleship(new Placement(new Coordinate(1, 2), 'H')));
    assertThrows(IllegalArgumentException.class,()->f.makeCarrier(new Placement(new Coordinate(1, 2), 'H')));
    
    Placement u0_0 = new Placement(new Coordinate(1, 2), 'U');
    Ship<Character> bat_u = f.makeBattleship(u0_0);
    Ship<Character> car_u = f.makeCarrier(u0_0);
  }

}
