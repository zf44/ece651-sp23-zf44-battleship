package edu.duke.zf44.battleship;

import static org.junit.jupiter.api.Assertions.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.parallel.Resources;
import org.junit.jupiter.api.Disabled;
//import org.junit.jupiter.api.parallel.Resources;
import org.junit.jupiter.api.Test;

public class TextPlayerTest {
  @Test
  public void test_read_placement() throws IOException {
    StringReader sr = new StringReader("B2V\nC8H\nA4V\n");
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    PrintStream ps = new PrintStream(bytes, true);
    Board<Character> b = new BattleShipBoard<Character>(10, 20,'X');
    TextPlayer player = new TextPlayer("A", b, new BufferedReader(sr), ps, new V1ShipFactory(),2,1);    
    //App app = new App(b, sr, ps);
    String prompt = "Please enter a location for a ship:";

    Placement[] expected = new Placement[3];
    expected[0] = new Placement(new Coordinate(1, 2), 'V');
    expected[1] = new Placement(new Coordinate(2, 8), 'H');
    expected[2] = new Placement(new Coordinate(0, 4), 'V');

    for (int i = 0; i < expected.length; i++) {
       Placement p = player.readPlacement(prompt);
       assertEquals(p, expected[i]); //did we get the right Placement back
       assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
       bytes.reset(); //clear out bytes for next time around
      }    
  }

  
  private TextPlayer createTextPlayer(int w, int h, String inputData, ByteArrayOutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h,'X');
    V1ShipFactory shipFactory = new V1ShipFactory();
    return new TextPlayer("A", board, input, output, shipFactory,2,1);
  }

  /***
  @Disabled
  @Test
  public void test_do_one_placement() throws IOException{
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(3, 3, "A0V\n", bytes);
    String prompt = "Where would you like to put your ship?";
    //player.doOnePlacement("Submarine",(p) -> shipFactory.makeSubmarine(p));
    String actual = bytes.toString();
    String expected = "Player A Where would you like to put your ship?\n";
    String expectedHeader = "  0|1|2\n";
    String expectedBody = "A s| |  A\n"+"B s| |  B\n"+"C s| |  C\n";
    expected = expected + expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected,actual);
  }

  @Disabled
  @Test
  public void test_do_placement_phase() throws IOException{
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(3, 3, "A0V\n", bytes);
    String prompt = "Where would you like to put your ship?";
    player.doPlacementPhase();
    String actual = bytes.toString();
    String emptyboard = "  0|1|2\n"+"A  | |  A\n"+"B  | |  B\n"+"C  | |  C\n"+"  0|1|2\n";
    String intro = "Player A: you are going to place the following ships (which are all rectangular). For each ship, type the coordinate of the upper left side of the ship, followed by either H (for horizontal) or V (for vertical).  For example M4H would place a ship horizontally starting at M4 and going to the right.  You have\n\n2 \"Submarines\" ships that are 1x2\n3 \"Destroyers\" that are 1x3\n3 \"Battleships\" that are 1x4\n2 \"Carriers\" that are 1x6\n";
    String expected = emptyboard + intro + "Player A Where would you like to put your ship?\n";
    String expectedHeader = "  0|1|2\n";
    String expectedBody = "A s| |  A\n"+"B s| |  B\n"+"C s| |  C\n";
    expected = expected + expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected,actual); 
  }

  @Disabled
  @Test
  public void test_do_placement_phase() throws IOException{
    StringReader sr = new StringReader("B2V\nC8H\nA4V\n");
    BufferedReader input = new BufferedReader(sr);
    Board<Character> b1 = new BattleShipBoard<Character>(3, 3);
    Board<Character> b2 = new BattleShipBoard<Character>(3, 3);
    V1ShipFactory factory = new V1ShipFactory();
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer p1 = new TextPlayer("A", b1, input, bytes, factory);
    TextPlayer p2 = new TextPlayer("B", b2, input, bytes, factory);
    App app = new App(p1,p2);
    app.doPlacementPhase();
    assertEquals(expectedStream,bytes.toString());
  }
  ***/
  @Test
  void test_read_empty_placement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "", bytes);
    assertThrows(EOFException.class, () -> player.readPlacement(""));
  }
  
  @Test
  void test_choose_empty_action() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    BufferedReader input = new BufferedReader(new StringReader(""));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(10, 20,'X');
    V2ShipFactory shipFactory = new V2ShipFactory();
    TextPlayer player = new TextPlayer("A", board, input, output, shipFactory,2,1);
    assertThrows(EOFException.class, () -> player.playOneTurn(board, "enemy"));
  }
  
  @Test
  void test_choose_illegal_action() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    BufferedReader input = new BufferedReader(new StringReader("B"));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(10, 20,'X');
    V2ShipFactory shipFactory = new V2ShipFactory();
    TextPlayer player = new TextPlayer("A", board, input, output, shipFactory,2,1);
    assertThrows(EOFException.class, () -> player.playOneTurn(board, "enemy"));
  }
  
  @Test
  void test_do_one_placement_error() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2V\nB2V\n", bytes);
    assertThrows(EOFException.class, () -> player.doPlacementPhase());
  }
}
