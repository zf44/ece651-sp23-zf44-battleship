package edu.duke.zf44.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import java.lang.*;
import java.util.ArrayList;
import java.util.*;
import java.io.*;

public class BattleShipBoardTest
{
   @Test
  public void test_width_and_height() {
     Board<Character> b1 = new BattleShipBoard(10, 20,'X');
    assertEquals(10, b1.getWidth());
    assertEquals(20, b1.getHeight());
  }

  @Test
  public void test_invalid_dimensions() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(10, 0,'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(0, 20,'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(10, -5,'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(-8, 20,'X'));
  }

  private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected){
    for (int i = 0; i<b.getHeight(); i++){
      for (int j = 0; j<b.getWidth(); j++){
        Coordinate c1 = new Coordinate(i,j);
        assertEquals(b.whatIsAtForSelf(c1),expected[i][j]);
      }
    }
  }

  @Test
  public void test_board(){
    Character[][] expected = new Character[5][3];
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(3,5,'X');
    checkWhatIsAtBoard(b1,expected);
  }

  @Test
  public void test_add_ship(){
    Character[][] expected = new Character[5][3];
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(3,5,'X');
    Coordinate c1 = new Coordinate(1,2);
    RectangleShip<Character> s1 = new RectangleShip<Character>(c1, 's', '*');
    //BasicShip s1 = new BasicShip(c1);
    Coordinate c2 = new Coordinate(2,2);
    assertEquals(null,b1.tryAddShip(s1));
    assertEquals('s',b1.whatIsAtForSelf(c1));
    assertEquals(null,b1.whatIsAtForSelf(c2));
    Coordinate c3 = new Coordinate(10,10);
    assertThrows(IllegalArgumentException.class, ()->b1.whatIsAtForSelf(c3));
  }

  @Test
  public void test_fire_at(){
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10,20,'X');
    Placement p1 = new Placement(new Coordinate(1,2),'V');
    Placement p2 = new Placement(new Coordinate(1,4),'V'); 
    V1ShipFactory f = new V1ShipFactory();
    Ship<Character> s1 = f.makeDestroyer(p1);
    Ship<Character> s2 = f.makeDestroyer(p2);                                                        
    b1.tryAddShip(s1);
    b1.tryAddShip(s2);
    NoCollisionRuleChecker<Character> noCollisionRuleChecker = new NoCollisionRuleChecker<>(null);
    InBoundsRuleChecker<Character> inBoundsRuleChecker = new InBoundsRuleChecker<>(noCollisionRuleChecker);
    assertThrows(IllegalArgumentException.class, ()->b1.fireAt(new Coordinate(-1,-1)));
    assertSame(s1,b1.fireAt(new Coordinate(1,2)));
    assertSame(s2,b1.fireAt(new Coordinate(1,4)));
    assertSame(null,b1.fireAt(new Coordinate(1,3)));
    assertSame(s1,b1.fireAt(new Coordinate(2,2)));
    assertEquals(false,s1.isSunk()); 
    assertSame(s1,b1.fireAt(new Coordinate(3,2)));
    assertEquals(true,s1.isSunk());
    
    BattleShipBoard<Character> b2 = new BattleShipBoard<Character>(10,20,'X');
    V2ShipFactory f2 = new V2ShipFactory();
    Ship<Character> s3 = f2.makeDestroyer(p1);
    b2.tryAddShip(s3);
    NoCollisionRuleChecker<Character> checker1 = new NoCollisionRuleChecker<>(null);
    InBoundsRuleChecker<Character> checker2 = new InBoundsRuleChecker<>(checker1);
    assertThrows(IllegalArgumentException.class, ()->b2.fireAt(new Coordinate(-1,-1)));
    assertSame(s3,b2.fireAt(new Coordinate(1,2)));
    
    Placement p3 = new Placement(new Coordinate(1,4),'U');
    Ship<Character> s4 = f2.makeCarrier(p3);
    NoCollisionRuleChecker<Character> checker3 = new NoCollisionRuleChecker<>(checker2);
    InBoundsRuleChecker<Character> checker4 = new InBoundsRuleChecker<>(checker3);
    assertEquals(null,checker3.checkPlacement(s4,b2));
    b2.tryAddShip(s4);
    assertEquals('c',b2.whatIsAtForSelf(new Coordinate(1,4)));
    InBoundsRuleChecker<Character> checker5 = new InBoundsRuleChecker<>(null);
    NoCollisionRuleChecker<Character> checker6 = new NoCollisionRuleChecker<>(checker5);
    assertEquals(null,checker5.checkPlacement(new Coordinate(1,4),b2));
    assertSame(s4,b2.fireAt(new Coordinate(1,4)));
    b2.fireAt(new Coordinate(1,4));
    //Coordinate upperLeft = new Coordinate(5,5);
    //Carrier<Character> s5 = new Carrier<Character>("carrier",upperLeft, 's','*','D');
    //assertSame(s5,b2.fireAt(new Coordinate(5,5)));
    
  }

  @Test
  public void test_what_is_at_for_enemy(){
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(4,5,'X');
    Placement p1 = new Placement(new Coordinate(1,2),'V');
    V1ShipFactory f = new V1ShipFactory();
    Ship<Character> s1 = f.makeDestroyer(p1);
    b1.tryAddShip(s1);
    b1.fireAt(new Coordinate(1,2));
    assertEquals('d',b1.whatIsAtForEnemy(new Coordinate(1,2)));
    assertEquals(null,b1.whatIsAtForEnemy(new Coordinate(1,3)));
    b1.fireAt(new Coordinate(2,3));
    assertEquals('X',b1.whatIsAtForEnemy(new Coordinate(2,3)));
  }

  @Test
  public void test_all_sunk(){
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(4,5,'X');
    V1ShipFactory f = new V1ShipFactory();
    Placement p1 = new Placement(new Coordinate(1,2),'V');
    Placement p2 = new Placement(new Coordinate(1,3),'V');
    Ship<Character> s1 = f.makeSubmarine(p1);
    Ship<Character> s2 = f.makeSubmarine(p2);
    b1.tryAddShip(s1);
    b1.tryAddShip(s2);
    b1.fireAt(new Coordinate(1,2));
    b1.fireAt(new Coordinate(2,2));
    assertEquals(false,b1.isAllSunk());
    b1.fireAt(new Coordinate(1,3));
    b1.fireAt(new Coordinate(2,3));
    assertEquals(true,b1.isAllSunk()); 
  }

    @Test
  public void test_sonar(){
    //BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10,20,'X');
    BattleShipBoard<Character> b2 = new BattleShipBoard<Character>(10,20,'X');
    V2ShipFactory f2 = new V2ShipFactory();
    
    Placement p3 = new Placement(new Coordinate(0,0),'U');
    Ship<Character> s4 = f2.makeBattleship(p3);
    b2.tryAddShip(s4);
    Placement p2 = new Placement("E3R");
    Ship<Character> s5 = f2.makeCarrier(p2);
    b2.tryAddShip(s5); 
    HashMap<String, Integer> result = b2.sonar(3, new Coordinate("D3"));
    assertEquals(0,result.get("Submarine"));
    assertEquals(0,result.get("Destroyer"));
    assertEquals(1,result.get("BattleShip"));
    assertEquals(4,result.get("Carrier"));

    assertThrows(IllegalArgumentException.class,()->b2.sonar(3, new Coordinate(-1,-1)));
    }
}
