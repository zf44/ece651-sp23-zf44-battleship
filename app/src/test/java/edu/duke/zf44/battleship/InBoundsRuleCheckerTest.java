package edu.duke.zf44.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
  @Test
  public void test_my_rules() {
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    V1ShipFactory f = new V1ShipFactory();
    Ship<Character> dst = f.makeDestroyer(v1_2);
    Board<Character> b1 = new BattleShipBoard<Character>(1, 2,'X');
    InBoundsRuleChecker<Character> checker1 = new InBoundsRuleChecker<Character>(null);
    assertEquals("That placement is invalid: the ship goes off the bottom of the board.",checker1.checkPlacement(dst,b1));
    
    InBoundsRuleChecker<Character> checker2 = new InBoundsRuleChecker<Character>(checker1);
    Board<Character> b2 = new BattleShipBoard<Character>(4, 6,'X');
    assertEquals(null,checker2.checkPlacement(dst,b2)); 
    
    Placement vm1_2 = new Placement(new Coordinate(-1, 2), 'V');
    Ship<Character> dst_2 = f.makeDestroyer(vm1_2);
    InBoundsRuleChecker<Character> checker3 = new InBoundsRuleChecker<Character>(checker2);
    assertEquals("That placement is invalid: the ship goes off the right of the board.",checker1.checkPlacement(dst_2,b1));
    
    Placement v1_3 = new Placement(new Coordinate(-1, -1), 'V');
    Ship<Character> dst_3 = f.makeDestroyer(v1_3);
    InBoundsRuleChecker<Character> checker4 = new InBoundsRuleChecker<Character>(checker3);
    assertEquals("That placement is invalid: the ship goes off the left of the board.",checker1.checkPlacement(dst_3,b2));
    
    Placement v1_m2 = new Placement(new Coordinate(-1, 2), 'H');
    Ship<Character> dst_4 = f.makeDestroyer(v1_m2);
    InBoundsRuleChecker<Character> checker5 = new InBoundsRuleChecker<Character>(checker4);
    assertEquals("That placement is invalid: the ship goes off the top of the board.",checker1.checkPlacement(dst_4,b2));
    
    Coordinate c1 = new Coordinate(-1, -1);
    InBoundsRuleChecker<Character> checker6 = new InBoundsRuleChecker<Character>(null);
    assertEquals("That placement is invalid: the ship goes off the top of the board.",checker1.checkPlacement(c1,b2));
  }

}
