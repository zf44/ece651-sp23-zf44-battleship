package edu.duke.zf44.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {
  @Test
  public void test_equals() {
    Placement p1 = new Placement("C1", 'v');
    Placement p2 = new Placement("C1", 'V');
    assertEquals(p1, p2);
    Placement p3 = new Placement("D2",'v');
    assertNotEquals(p1,p3);
    assertNotEquals(p1,"((1, 2), V)");
  }

  @Test
  public void test_hashCode(){
    Placement p1 = new Placement("C1",'v');
    Placement p2 = new Placement("C1",'V');
    Placement p3 = new Placement("D2",'v');
    assertEquals(p1.hashCode(),p2.hashCode());
    assertNotEquals(p1.hashCode(),p3.hashCode());
  }

  @Test
  public void test_where_orientation(){
    Placement p1 = new Placement("C1",'v');
    Coordinate c1 = new Coordinate("C1");
    assertEquals(c1,p1.getWhere());
    assertEquals('V',p1.getOrientation());
  }

  @Test
  void test_string_constructor_valid_cases(){
    Placement p1 = new Placement("B3V");
    Coordinate c1 = new Coordinate("B3");
    assertEquals(c1,p1.getWhere());
    assertEquals('V',p1.getOrientation());
  }

  @Test
  void test_string_constructor_error_cases(){
    assertThrows(IllegalArgumentException.class, () -> new Placement("B3"));
    //assertThrows(IllegalArgumentException.class, () -> new Placement("B3T"));
  }
}
