package edu.duke.zf44.battleship;
import java.lang.*;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
  @Test
    public void test_display_empty_2by2() {

    Board<Character> b1 = new BattleShipBoard(2, 2,'X');
    BoardTextView view = new BoardTextView(b1);
    String expectedHeader= "  0|1\n";
    assertEquals(expectedHeader, view.makeHeader());
    String expected=
      expectedHeader+
      "A  |  A\n"+
      "B  |  B\n"+
      expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }
  @Test
  public void test_invalid_board_size() {
    Board<Character> wideBoard = new BattleShipBoard(11,20,'X');
    Board<Character> tallBoard = new BattleShipBoard(10,27,'X');
    //you should write two assertThrows here
     assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
     assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
  }
  
  private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
    Board<Character> b1 = new BattleShipBoard(w, h,'X');
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + expectedBody  + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

 @Test
 public void test_display_empty_3by2(){
   String expectedHeader= "  0|1|2\n";
   String expectedBody = "A  | |  A\n"+
                         "B  | |  B\n";
   emptyBoardHelper(3,2,expectedHeader,expectedBody);
 }

 @Test
 public void test_display_empty_3by5(){
   String expectedHeader="  0|1|2\n";
   String expectedBody="A  | |  A\n"+
                       "B  | |  B\n"+
                       "C  | |  C\n"+
                       "D  | |  D\n"+
                       "E  | |  E\n";
   emptyBoardHelper(3,5,expectedHeader,expectedBody);
 }

 @Test
 public void test_ship_display(){
   Coordinate c1 = new Coordinate("B1");
   RectangleShip<Character> s1 = new RectangleShip<Character>(c1, 's', '*');
   //BasicShip s1 = new BasicShip(c1);
   BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(3,4,'X');
   b1.tryAddShip(s1);
   String expectedHeader="  0|1|2\n";
   String expectedBody="A  | |  A\n"+
                       "B  |s|  B\n"+
                       "C  | |  C\n"+
                       "D  | |  D\n";
   String expected = expectedHeader + expectedBody  + expectedHeader;
   BoardTextView view = new BoardTextView(b1);
   assertEquals(expected, view.displayMyOwnBoard());
 }

  @Test
  public void test_display_enemy_board(){
    BattleShipBoard<Character> b1 = new BattleShipBoard<>(3, 4, 'X');
    V1ShipFactory f = new V1ShipFactory();
    Placement h1_0 = new Placement(new Coordinate(1, 0), 'H');
    Ship<Character> s1 = f.makeSubmarine(h1_0);
    b1.tryAddShip(s1);
    BoardTextView view = new BoardTextView(b1);
    String expectedSelf = "  0|1|2\n"+ 
                          "A  | |  A\n"+
                          "B s|s|  B\n"+
                          "C  | |  C\n"+
                          "D  | |  D\n"+
                          "  0|1|2\n";
    String expectedEmeny = "  0|1|2\n"+ 
                          "A  | |  A\n"+
                          "B  | |  B\n"+
                          "C  | |  C\n"+
                          "D  | |  D\n"+
                          "  0|1|2\n";
    assertEquals(expectedSelf, view.displayMyOwnBoard());
    assertEquals(expectedEmeny, view.displayEnemyBoard());
    
    Coordinate c1 = new Coordinate(1, 0);
    b1.fireAt(c1);
    expectedEmeny = "  0|1|2\n"+ 
                    "A  | |  A\n"+
                    "B s| |  B\n"+
                    "C  | |  C\n"+
                    "D  | |  D\n"+
                    "  0|1|2\n";
    assertEquals(expectedEmeny, view.displayEnemyBoard());
    
    Coordinate c2 = new Coordinate(0, 1);
    b1.fireAt(c2);
    expectedEmeny = "  0|1|2\n"+ 
                    "A  |X|  A\n"+
                    "B s| |  B\n"+
                    "C  | |  C\n"+
                    "D  | |  D\n"+
                    "  0|1|2\n";
    assertEquals(expectedEmeny, view.displayEnemyBoard());
  }
  
  @Test
  public void test_display_both_board(){
    BattleShipBoard<Character> myB = new BattleShipBoard<>(3, 4, 'X');
    BattleShipBoard<Character> enemyB = new BattleShipBoard<>(3, 4, 'X');
    BoardTextView myV = new BoardTextView(myB);
    BoardTextView enemyV = new BoardTextView(enemyB);
    String expected = "     Your ocean             Player B's ocean\n"+
                            "  0|1|2                    0|1|2\n"+
                            "A  | |  A                A  | |  A\n"+
                            "B  | |  B                B  | |  B\n"+
                            "C  | |  C                C  | |  C\n"+
                            "D  | |  D                D  | |  D\n"+
                            "  0|1|2                    0|1|2\n";
    assertEquals(expected, myV.displayMyBoardWithEnemyNextToIt(enemyV, "Your ocean", "Player B's ocean"));
    
    V1ShipFactory f = new V1ShipFactory();
    Placement v0_0 = new Placement(new Coordinate(0, 0), 'V');
    Ship<Character> s1 = f.makeSubmarine(v0_0);
    Ship<Character> s2 = f.makeSubmarine(v0_0);
    myB.tryAddShip(s1);
    enemyB.tryAddShip(s2);
    expected = "     Your ocean             Player B's ocean\n"+
                      "  0|1|2                    0|1|2\n"+
                      "A s| |  A                A  | |  A\n"+
                      "B s| |  B                B  | |  B\n"+
                      "C  | |  C                C  | |  C\n"+
                      "D  | |  D                D  | |  D\n"+
                      "  0|1|2                    0|1|2\n";
    assertEquals(expected, myV.displayMyBoardWithEnemyNextToIt(enemyV, "Your ocean", "Player B's ocean"));
    
    enemyB.fireAt(new Coordinate(1, 0));
    enemyB.fireAt(new Coordinate(2, 0));
    expected = "     Your ocean             Player B's ocean\n"+
                      "  0|1|2                    0|1|2\n"+
                      "A s| |  A                A  | |  A\n"+
                      "B s| |  B                B s| |  B\n"+
                      "C  | |  C                C X| |  C\n"+
                      "D  | |  D                D  | |  D\n"+
                      "  0|1|2                    0|1|2\n";
    assertEquals(expected, myV.displayMyBoardWithEnemyNextToIt(enemyV, "Your ocean", "Player B's ocean"));
  }
  
  @Test
  public void test_new_battleship_carrier(){
    BattleShipBoard<Character> b1 = new BattleShipBoard<>(3, 5, 'X');
    V2ShipFactory f = new V2ShipFactory();
    Placement u0_0 = new Placement(new Coordinate(0,0), 'U');
    Ship<Character> car_u = f.makeCarrier(u0_0);
    b1.tryAddShip(car_u);
    BoardTextView view = new BoardTextView(b1);
    String expected = "  0|1|2\n"+ 
                      "A c| |  A\n"+
                      "B c| |  B\n"+
                      "C c|c|  C\n"+
                      "D c|c|  D\n"+
                      "E  |c|  E\n"+
                      "  0|1|2\n";
    assertEquals(expected, view.displayMyOwnBoard());
  }
  
  @Test
  public void test_move(){
    BattleShipBoard<Character> b1 = new BattleShipBoard<>(3, 5, 'X');
    V2ShipFactory f = new V2ShipFactory();
    Placement u0_0 = new Placement(new Coordinate(0,0), 'U');
    Ship<Character> car_u = f.makeCarrier(u0_0);
    assertEquals(true, car_u.occupiesCoordinates(new Coordinate(0,0)));
    b1.tryAddShip(car_u);
    String expected = "  0|1|2\n"+ 
                      "A c| |  A\n"+
                      "B c| |  B\n"+
                      "C c|c|  C\n"+
                      "D c|c|  D\n"+
                      "E  |c|  E\n"+
                      "  0|1|2\n";
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expected, view.displayMyOwnBoard());
    Placement v0_2 = new Placement(new Coordinate(0,2), 'V');
    Ship<Character> sub1 = f.makeSubmarine(v0_2);
    b1.tryAddShip(sub1);
    expected = "  0|1|2\n"+ 
               "A c| |s A\n"+
               "B c| |s B\n"+
               "C c|c|  C\n"+
               "D c|c|  D\n"+
               "E  |c|  E\n"+
               "  0|1|2\n";
    assertEquals(expected, view.displayMyOwnBoard());
    assertEquals(true, car_u.occupiesCoordinates(new Coordinate(0,0)));
    assertSame(sub1,b1.fireAt(new Coordinate("A2")));
    assertSame(car_u,b1.getShip(new Coordinate(0,0)));
    assertEquals(true, car_u.occupiesCoordinates(new Coordinate(0,0)));
    b1.fireAt(new Coordinate(0,0));
    Placement u0_1 = new Placement(new Coordinate(0,1), 'U');
    Ship<Character> car_dst = f.makeCarrier(u0_1);
    b1.tryMoveShip(car_u,car_dst);
    //BoardTextView view = new BoardTextView(b1);
    expected = "  0|1|2\n"+ 
               "A  |*|* A\n"+
               "B  |c|s B\n"+
               "C  |c|c C\n"+
               "D  |c|c D\n"+
               "E  | |c E\n"+
               "  0|1|2\n";
    assertEquals(expected, view.displayMyOwnBoard());
    assertSame(null,b1.getShip(new Coordinate(0,0)));
    
    Placement u1_0 = new Placement(new Coordinate(-1,0), 'U');
    Ship<Character> car_dst_1 = f.makeCarrier(u1_0);
    InBoundsRuleChecker<Character> checker1 = new InBoundsRuleChecker<Character>(null);
    assertThrows(IllegalArgumentException.class,()->b1.tryMoveShip(car_dst,car_dst_1));
    
    Placement u2_0 = new Placement(new Coordinate(0,2), 'U');
    Ship<Character> car_dst_2 = f.makeCarrier(u2_0);
    NoCollisionRuleChecker<Character> checker3 = new NoCollisionRuleChecker<Character>(checker1);
    assertThrows(IllegalArgumentException.class,()->b1.tryMoveShip(car_dst_1,car_dst_2));
    
    Placement v2_2 = new Placement(new Coordinate(2,2), 'V');
    Ship<Character> sub1_1 = f.makeSubmarine(v2_2);
    NoCollisionRuleChecker<Character> checker4 = new NoCollisionRuleChecker<Character>(checker3);
    assertThrows(IllegalArgumentException.class,()->b1.tryMoveShip(sub1,sub1_1));
  }
  
  @Test
  public void test_fire_carrier(){
    BattleShipBoard<Character> b2 = new BattleShipBoard<Character>(6,5,'X');
    V2ShipFactory f2 = new V2ShipFactory();
    
    Placement p3 = new Placement(new Coordinate(0,0),'U');
    Ship<Character> s4 = f2.makeCarrier(p3);
    b2.tryAddShip(s4);
    b2.fireAt(new Coordinate(0,0));
    
    BoardTextView view = new BoardTextView(b2);
    String expected = "  0|1|2|3|4|5\n"+ 
                      "A *| | | | |  A\n"+
                      "B c| | | | |  B\n"+
                      "C c|c| | | |  C\n"+
                      "D c|c| | | |  D\n"+
                      "E  |c| | | |  E\n"+
                      "  0|1|2|3|4|5\n"; 
    assertEquals(expected, view.displayMyOwnBoard());
    
    Placement u0_1 = new Placement(new Coordinate(0,0), 'R');
    Ship<Character> car_dst = f2.makeCarrier(u0_1);   
    b2.tryMoveShip(s4,car_dst);
    
     expected = "  0|1|2|3|4|5\n"+ 
                "A  |c|c|c|*|  A\n"+
                "B c|c|c| | |  B\n"+
                "C  | | | | |  C\n"+
                "D  | | | | |  D\n"+
                "E  | | | | |  E\n"+
                "  0|1|2|3|4|5\n";
    assertEquals(expected, view.displayMyOwnBoard());
    
    Placement d0_0 = new Placement(new Coordinate(0,0), 'D');
    Ship<Character> car_dst_1 = f2.makeCarrier(d0_0);   
    b2.tryMoveShip(car_dst,car_dst_1);
     expected = "  0|1|2|3|4|5\n"+ 
            "A c| | | | |  A\n"+
            "B c|c| | | |  B\n"+
            "C c|c| | | |  C\n"+
            "D  |c| | | |  D\n"+
            "E  |*| | | |  E\n"+
            "  0|1|2|3|4|5\n";
    assertEquals(expected, view.displayMyOwnBoard());
            
    Placement l0_0 = new Placement(new Coordinate(0,0), 'L');
    Ship<Character> car_dst_2 = f2.makeCarrier(l0_0);   
    b2.tryMoveShip(car_dst_1,car_dst_2);
    expected = "  0|1|2|3|4|5\n"+ 
            "A  | |c|c|c|  A\n"+
            "B *|c|c|c| |  B\n"+
            "C  | | | | |  C\n"+
            "D  | | | | |  D\n"+
            "E  | | | | |  E\n"+
            "  0|1|2|3|4|5\n";
    assertEquals(expected, view.displayMyOwnBoard());
  }
  
  @Test
  public void test_move_battleship(){
    BattleShipBoard<Character> b2 = new BattleShipBoard<Character>(6,5,'X');
    V2ShipFactory f2 = new V2ShipFactory();
    
    Placement p3 = new Placement(new Coordinate(0,0),'U');
    Ship<Character> s4 = f2.makeBattleship(p3);
    b2.tryAddShip(s4);
    b2.fireAt(new Coordinate(1,0));
    
    BoardTextView view = new BoardTextView(b2);
    String expected = "  0|1|2|3|4|5\n"+ 
                      "A  |b| | | |  A\n"+
                      "B *|b|b| | |  B\n"+
                      "C  | | | | |  C\n"+
                      "D  | | | | |  D\n"+
                      "E  | | | | |  E\n"+
                      "  0|1|2|3|4|5\n"; 
    assertEquals(expected, view.displayMyOwnBoard());
    
    Placement u0_1 = new Placement(new Coordinate(0,0), 'R');
    Ship<Character> car_dst = f2.makeBattleship(u0_1);   
    b2.tryMoveShip(s4,car_dst);
    
     expected = "  0|1|2|3|4|5\n"+ 
                "A *| | | | |  A\n"+
                "B b|b| | | |  B\n"+
                "C b| | | | |  C\n"+
                "D  | | | | |  D\n"+
                "E  | | | | |  E\n"+
                "  0|1|2|3|4|5\n";
    assertEquals(expected, view.displayMyOwnBoard());
    
    Placement d0_0 = new Placement(new Coordinate(0,0), 'D');
    Ship<Character> car_dst_1 = f2.makeBattleship(d0_0);   
    b2.tryMoveShip(car_dst,car_dst_1);
     expected = "  0|1|2|3|4|5\n"+ 
            "A b|b|*| | |  A\n"+
            "B  |b| | | |  B\n"+
            "C  | | | | |  C\n"+
            "D  | | | | |  D\n"+
            "E  | | | | |  E\n"+
            "  0|1|2|3|4|5\n";
    assertEquals(expected, view.displayMyOwnBoard());
            
    Placement l0_0 = new Placement(new Coordinate(0,0), 'L');
    Ship<Character> car_dst_2 = f2.makeBattleship(l0_0);   
    b2.tryMoveShip(car_dst_1,car_dst_2);
    expected = "  0|1|2|3|4|5\n"+ 
            "A  |b| | | |  A\n"+
            "B b|b| | | |  B\n"+
            "C  |*| | | |  C\n"+
            "D  | | | | |  D\n"+
            "E  | | | | |  E\n"+
            "  0|1|2|3|4|5\n";
    assertEquals(expected, view.displayMyOwnBoard());
  }
}
