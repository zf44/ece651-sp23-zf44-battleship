package edu.duke.zf44.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class NoCollisionRuleCheckerTest {
  @Test
  public void test_no_collision() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20,'X');
    V1ShipFactory f = new V1ShipFactory();
    
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst = f.makeDestroyer(v1_2);
    NoCollisionRuleChecker<Character> checker1 = new NoCollisionRuleChecker<Character>(null);
    assertEquals(null,checker1.checkPlacement(dst,b1));
    InBoundsRuleChecker<Character> checker3 = new InBoundsRuleChecker<Character>(checker1);
    assertEquals(null,checker3.checkPlacement(dst,b1));
    b1.tryAddShip(dst);
    Placement h1_2 = new Placement(new Coordinate(1, 2), 'H');
    Ship<Character> dst_h = f.makeDestroyer(h1_2);
    NoCollisionRuleChecker<Character> checker2 = new NoCollisionRuleChecker<Character>(null);
    assertEquals("That placement is invalid: the ship overlaps another ship.",checker2.checkPlacement(dst,b1));
    assertEquals("That placement is invalid: the ship overlaps another ship.",b1.tryAddShip(dst_h));
    //InBoundsRuleChecker<Character> checker3 = new InBoundsRuleChecker<Character>(checker1);
    //assertEquals(true,checker3.checkPlacement(dst,b1));
  }
  

}
