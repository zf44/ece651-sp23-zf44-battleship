package edu.duke.zf44.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import java.util.*;
public class RectangleShipTest {
  @Test
  public void test_set() {
    Coordinate upperLeft = new Coordinate(1,2);
    RectangleShip<Character> rec1 = new RectangleShip<Character>("submarine",upperLeft,1,3, new SimpleShipDisplayInfo<Character>('s', '*'),new SimpleShipDisplayInfo<Character>(null, 's'));
    assertEquals(true,rec1.occupiesCoordinates(new Coordinate(1,2)));
    assertEquals("submarine",rec1.getName());
  }

  @Test
  public void test_hit(){
    Coordinate upperLeft = new Coordinate(1,2);
    RectangleShip<Character> rec = new RectangleShip<Character>("submarine",upperLeft,1,3, new SimpleShipDisplayInfo<Character>('s','*'),new SimpleShipDisplayInfo<Character>(null, 's'));
    Coordinate hit1 = new Coordinate(1,2);
    rec.recordHitAt(hit1);
    assertThrows(IllegalArgumentException.class, () ->rec.wasHitAt(new Coordinate(6,2)));
    assertEquals(true,rec.wasHitAt(hit1));
  }

  @Test
  public void test_sunk(){
    Coordinate upperLeft = new Coordinate(1,2);
    RectangleShip<Character> rec = new RectangleShip<Character>("submarine",upperLeft,1,3, new SimpleShipDisplayInfo<Character>('s','*'),new SimpleShipDisplayInfo<Character>(null, 's'));
    Coordinate hit1 = new Coordinate(1,2);
    rec.recordHitAt(hit1);
    assertEquals(false,rec.isSunk());
    Coordinate hit2 = new Coordinate(2,2);
    rec.recordHitAt(hit2);
    assertEquals(false,rec.isSunk());
    Coordinate hit3 = new Coordinate(3,2);
    rec.recordHitAt(hit3);
    assertEquals(true,rec.isSunk());
  }

  @Test
  public void test_display(){
    Coordinate upperLeft = new Coordinate(1,2);
    RectangleShip<Character> rec = new RectangleShip<Character>("submarine",upperLeft,1,3, new SimpleShipDisplayInfo<Character>('s','*'),new SimpleShipDisplayInfo<Character>(null, 's'));
    Coordinate hit1 = new Coordinate(1,2);
    //rec.recordHitAt(hit1);
    Coordinate hit2 = new Coordinate(2,2);
    //assertEquals('*',rec.getDisplayInfoAt(hit1));
    assertEquals('s',rec.getDisplayInfoAt(hit2,true));
  }

  @Test
  public void test_get_coordinates(){
    Coordinate upperLeft = new Coordinate(1,2);
    RectangleShip<Character> rec = new RectangleShip<Character>("Submarine",upperLeft,1,2, new SimpleShipDisplayInfo<Character>('s','*'),new SimpleShipDisplayInfo<Character>(null, 's'));
    Set<Coordinate> expected = new HashSet<Coordinate>();
    Coordinate c1 = new Coordinate(1,2);
    Coordinate c2 = new Coordinate(2,2);
    expected.add(c1);
    expected.add(c2);
    assertEquals(expected,rec.getCoordinates());
    assertEquals(upperLeft,rec.getUpperLeft());
    assertEquals('N',rec.getOrientation());
    assertEquals("rec",rec.getType());
  }
}

