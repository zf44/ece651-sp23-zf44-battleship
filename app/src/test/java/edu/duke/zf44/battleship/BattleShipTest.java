package edu.duke.zf44.battleship;

import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BattleShipTest {
  @Test
  public void test_battleship() {
    Coordinate upperLeft = new Coordinate(0,0);
    BattleShip<Character> bat1 = new BattleShip<Character>("battleShip",upperLeft, new SimpleShipDisplayInfo<Character>('s', '*'),new SimpleShipDisplayInfo<Character>(null, 's'),'U');
    assertEquals(false,bat1.occupiesCoordinates(new Coordinate(0,0)));
    assertEquals(true,bat1.occupiesCoordinates(new Coordinate(0,1)));
    assertEquals(true,bat1.occupiesCoordinates(new Coordinate(1,0)));
    assertEquals(true,bat1.occupiesCoordinates(new Coordinate(1,1)));
    assertEquals(true,bat1.occupiesCoordinates(new Coordinate(1,2)));

    BattleShip<Character> bat2 = new BattleShip<Character>("battleShip",upperLeft, new SimpleShipDisplayInfo<Character>('s', '*'),new SimpleShipDisplayInfo<Character>(null, 's'),'R');
    assertEquals(true,bat2.occupiesCoordinates(new Coordinate(0,0)));
    assertEquals(true,bat2.occupiesCoordinates(new Coordinate(1,0)));
    assertEquals(true,bat2.occupiesCoordinates(new Coordinate(2,0)));
    assertEquals(true,bat2.occupiesCoordinates(new Coordinate(1,1)));
    
    BattleShip<Character> bat3 = new BattleShip<Character>("battleShip",upperLeft, new SimpleShipDisplayInfo<Character>('s', '*'),new SimpleShipDisplayInfo<Character>(null, 's'),'D');
    assertEquals(true,bat3.occupiesCoordinates(new Coordinate(0,0)));
    assertEquals(true,bat3.occupiesCoordinates(new Coordinate(0,1)));
    assertEquals(true,bat3.occupiesCoordinates(new Coordinate(0,2)));
    assertEquals(true,bat3.occupiesCoordinates(new Coordinate(1,1)));
    
    BattleShip<Character> bat4 = new BattleShip<Character>("battleShip",upperLeft, 's','*','L');
    assertEquals(true,bat4.occupiesCoordinates(new Coordinate(0,1)));
    assertEquals(true,bat4.occupiesCoordinates(new Coordinate(1,0)));
    assertEquals(true,bat4.occupiesCoordinates(new Coordinate(1,1)));
    assertEquals(true,bat4.occupiesCoordinates(new Coordinate(2,1)));
    
    assertEquals("battleShip",bat1.getName());
    assertEquals(upperLeft,bat1.getUpperLeft());
    assertEquals("battleShip",bat1.getType());
    assertEquals('R',bat2.getOrientation());
  }

}
