package edu.duke.zf44.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BasicShipTest {
  @Test
  public void test_occupies_coordinates() {
    Coordinate c1 = new Coordinate("B2");
    RectangleShip<Character> b1 = new RectangleShip<Character>(c1, 's', '*');
    //BasicShip b1 = new BasicShip(c1);
    Coordinate c2 = new Coordinate("B2");
    assertEquals(true,b1.occupiesCoordinates(c2));
  }

  @Test
  public void test_get_display_infoat(){
    Coordinate c1 = new Coordinate("B2");
    RectangleShip<Character> b1 = new RectangleShip<Character>(c1, 's', '*');
    //BasicShip b1 = new BasicShip(c1);
    Coordinate c2 = new Coordinate("B2");
    assertEquals('s',b1.getDisplayInfoAt(c2,true));
  }
}
